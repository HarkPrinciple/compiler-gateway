using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

using HarkLib.Core;

namespace HarkLib.CompilerGateway
{
    public static class Program
    {
        private static Compilation GetFromName(FileSettings settings, string name)
        {
            string lname = name.ToLower();
            
            if(lname.StartsWith("cs"))
                return new CompilationCs(settings, name.Substring(3));
            else if(lname.StartsWith("fs"))
                return new CompilationFs(settings, name.Substring(3));
            else
                throw new Exception("Can't parse the project \"" + name + "\"!");
        }

        private static bool Delete(string path)
        {
            try
            {
                if(File.Exists(path))
                {
                    Console.WriteLine(" ::File:: {0}", path);
                    File.Delete(path);
                }
                else if(Directory.Exists(path))
                {
                    Console.WriteLine(" ::Folder:: {0}", path);
                    Directory.Delete(path, true);
                }
                
                return true;
            }
            catch(IOException ex)
            {
                Console.Error.WriteLine(" /!\\ {0}", ex.Message);
                return false;
            }
        }
        
        public static void Main(string[] args)
        {
            string settingsFile = ".make/config.ini";
            if(!File.Exists(settingsFile))
            {
                Console.WriteLine(" [ ] Generating a config file.");
                using(Stream stream = File.Open(settingsFile, FileMode.CreateNew))
                using(StreamWriter sw = new StreamWriter(stream))
                {
                    sw.Write(@"#=======================~~~~~~~~~~~~~~~~~~~~=======================#
#===                                                            ===#
#==   /[]    []\      /[][][]\     /[][][][][]\    [][]   /[][]  ==#
#=   [][]\  /[][]    /[]    []\    [][]     [][]   [][] /[][]/    =#
#=~~~[][==oo==][]~~~/][]~~~~[][\~~~[=o][][=o=]/~~~~[][=o=]~~~~~~~~=#
#=   [][]/  \[][]   [][==oo==][]   [][] \[][]\     [][] \[][]\    =#
#==   \[]    []/    [=o]    [o=]   [=o]    \[][]   [=o]   \[o=]  ==#
#===                                                            ===#
#=======================~~~~~~~~~~~~~~~~~~~~==============[v1.0.0]=#
#=========================                =========================#
#============                                          ============#
#=======                 Configuration file                 =======#
#===== ------------------------------------------------------ =====#
#====                                                          ====#
#===   # = comment                                              ===#
#==    | = array separator                                       ==#
#==    %(USER) = user name                                       ==#
#==    %(HOME) = home directory                                  ==#
#==    $(OBJ) = replace by the content of OBJ                    ==#
#===                                                            ===#
#=== ---------------------------------------------------------- ===#
#=====                                                        =====#
#============              Hark Principle              ============#
##========================                ========================##
####============================================================####


########## Global settings ##########

### Files to clean
# : Array
Clean = $(CacheFile) | $(OutputFiles)

CacheFile = .make/.make-cache
UseCache = yes

ShowErrorCommandLine = no

# Files to clean <internal>
OutputFiles = $(Cs-Project-DestinationPath)/$(Cs-Project-OutputName)

# Compilers <internal>
Cs-CompilerName = csc.exe
Fs-CompilerName = fsc.exe

# Source path <internal>
SourcePath = src

# Output path <internal>
OutputFolder = out

# Dependencies path <internal>
Dependencies = dep

Fs-CommonFolder = $(SourcePath)/FsCommon

### Projects to compile (property name)
# : Array
# The order is important
# Cs = C# ; Fs = F#
Projects = Cs-Project

# surface | complet
# surface = only seek files on the directory
# complet = seek for files in subfolders too
Fs-CommonFolderType = surface

########## Projects settings ##########
# $(Cs-CompilerName) /out:{DEST}
#                    /nowarn:3013
#                    /target:{TARGET}
#                    {FILES}
#                    {MODULES}
#                    {DOC}
#                    /nologo
#                    {ADDON}
#                    {REFERENCES}
#
# Cs-{NAME}-OutputName = {VALUE}
#       ...-SourcePath
#       ...-DestinationPath
#       ...-GenerateDoc
#       ...-DocFullPath
#       ...-References
#       ...-Modules
#       ...-Target
#       ...-Addon
#
# $(Fs-CompilerName) --out:{DEST}
#                    --tailcalls+
#                    --target:{TARGET}
#                    {FILES}
#                    {DOC}
#                    --nologo
#                    {ADDON}
#                    {REFERENCES}
#
# Fs-{NAME}-OutputName = {VALUE}
#       ...-SourcePath
#       ...-DestinationPath
#       ...-GenerateDoc
#       ...-DocFullPath
#       ...-References
#       ...-Target
#       ...-Addon

Cs-Project-OutputName = Project.exe
Cs-Project-SourcePath = $(SourcePath)
Cs-Project-DestinationPath = $(OutputFolder)
Cs-Project-Target = exe

#=== ---------------------------------------------------------- ===#
#=====                                                        =====#
#============                                          ============#
#=========================                =========================#
#==================================================================#
####################################################################");
                }

                Console.WriteLine(" [o] Config file generate at \"{0}\".", settingsFile);
                return;
            }

            FileSettings settings = new FileSettings(settingsFile);

            if(args.Length == 0)
            {
                DisplayHelp();
                return;
            }

            foreach(string arg in args)
            {
                switch(arg.Trim().ToLower())
                {
                    case "make":
                    case "build":
                    case "clean":
                        break;
                    
                    default:
                        Console.Error.WriteLine(" [!] Can't understand the command \"{0}\".", arg);
                        return;
                }
            }

            foreach(string arg in args)
            {
                switch(arg.Trim().ToLower())
                {
                    case "clean":
                        Console.WriteLine(" [ ] Clearing...");
                        
                        bool result = settings
                            .GetList("Clean")
                            .Select(Delete)
                            .Aggregate(true, (a,b) => a && b);
                            
                        if(result)
                            Console.WriteLine(" [o] Cleaned.");
                        else
                            Console.Error.WriteLine(" [!] An error occured.");
                        break;

                    case "make":
                    case "build":
                        Cache.Instance = new Cache(settings);
                        
                        settings.GetList("Projects")
                            .Select(s => GetFromName(settings, s))
                            .ToList()
                            .ForEach(c => c.Execute());
                        
                        Cache.Instance.Save();
                        break;
                }
            }
        }

        public static void DisplayHelp()
        {
            ConsoleManager.DisplayHelp(
                version : "1.0.0",
                values : new Dictionary<string, IDictionary<string, string>>()
                {
                    { "Commands", new Dictionary<string, string>()
                        {
                            { "clean", "Clean the output folder and the cache" },
                            { "make / build", "Build the project" }
                        }
                    }
                }
            );
        }
    }
}